package com.kgc.service;

import com.kgc.entity.Book;
import com.kgc.entity.BookType;
import com.kgc.entity.ShoppingCar;

import java.util.List;

public interface BookService {
    //查询所有图书
    List<Book> queryBook();
    //增加图书
    int addBook(Book book);
    //查询所有图书分类
    List<BookType> queryBookType();
    //通过id删除所对应的图书
    int delBookById(int id);
    //通过id查询对应图书的信息
    Book queryBookById(int id);
    //通过id更新该图书
    int updateBookById(Book book);
    //通过分类获取book
    List<Book> queryBookByType(int bookType);
    //admin -- 通过关键词搜索图书
    List<Book> queryBookBySearch(String searchBook);
    //更改图书库存
    int updataBookNumber(Integer bookid,Integer bookNumber);


}
