package com.kgc.service.impl;

import com.kgc.entity.ShippingAddress;
import com.kgc.mapper.ShippingAddressMapper;
import com.kgc.service.ShippingAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShippingAddressServiceImpl  implements ShippingAddressService {

    @Autowired
    private ShippingAddressMapper shippingAddressMapper;

    @Override
    public int queryCountByUserId(Integer userid) {
        return shippingAddressMapper.queryCountByUserId(userid);
    }

    @Override
    public List<ShippingAddress> queryAddressById(Integer userid) {
        return shippingAddressMapper.queryAddressById(userid);
    }

    @Override
    public int addAddress(ShippingAddress address) {
        return shippingAddressMapper.addAddress(address);
    }

    @Override
    public ShippingAddress queryAddressByAddressId(Integer id) {
        return shippingAddressMapper.queryAddressByAddressId(id);
    }

    @Override
    public int delAddressById(int id) {
        return shippingAddressMapper.delAddressById(id);
    }

    @Override
    public int updateAddressById(ShippingAddress address) {
        return shippingAddressMapper.updateAddressById(address);
    }

}
