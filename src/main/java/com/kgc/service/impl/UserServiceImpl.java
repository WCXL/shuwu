package com.kgc.service.impl;

import com.kgc.entity.*;
import com.kgc.mapper.UserMapper;
import com.kgc.service.UserService;
import com.kgc.utils.MD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    /**
     * 根据用户名和密码查询用户
     * @param tel
     * @param password
     * @return
     */
    @Override
    public User queryByNameAndPassword(String tel, String password) {
        User user = userMapper.queryByNameAndPassword(tel,MD5.string2MD5(password));
        if(user!=null){
            user.setPassword("");
        }
        return user;
    }

    /**
     * 根据手机号查询用户
     * @param tel
     * @return
     */
    @Override
    public User queryByTel(String tel) {
        User user = userMapper.queryByTel(tel);
        return user;
    }

    /**
     * 注册
     * @param user
     * @return
     */
    @Override
    public int register(User user) {
        user.setPassword(MD5.string2MD5(user.getPassword()));
        return userMapper.register(user);
    }

    /**
     * 查询所有用户信息
     * @return
     */
    @Override
    public List<User> queryUser() {
        return userMapper.queryUser();
    }

    /**
     * 查询用户所有类型
     * @return
     */
    @Override
    public List<UserRole> queryUserRole() {
        return userMapper.queryUserRole();
    }

    /**
     * 控制台增加用户
     * @param user
     * @return
     */
    @Override
    public int adduser(User user) {
        user.setPassword(MD5.string2MD5(user.getPassword()));
        return userMapper.adduser(user);
    }

    /**
     * 通过id删除该条用户
     * @param id
     * @return
     */
    @Override
    public int deluser(int id) {
        return userMapper.deluser(id);
    }

    /**
     * 通过id查找对应用户信息
     * @param id
     * @return
     */
    @Override
    public User queryUserById(int id) {
        return userMapper.queryUserById(id);
    }

    /**
     * 通过id更新该用户
     * @param user
     * @return
     */
    @Override
    public int updateUser(User user) {
        return userMapper.updateuser(user);
    }

    @Override
    public int completeInfo(User user) {
        return userMapper.completeInfo(user);
    }

    @Override
    public List<ShippingAddress> queryAddress(int id) {
        return userMapper.queryAddress(id);
    }

    @Override
    public int updatePassword(int id, String password) {
        return userMapper.updatePassword(id,MD5.string2MD5(password));
    }

    @Override
    public List<User> admin_searchUser(String searchUser) {
        return userMapper.admin_searchUser(searchUser);
    }

    @Override
    public boolean updateHead(String headPath, int id) {
        return userMapper.updateHead(headPath,id)>=1?true:false;
    }

    @Override
    public User queryIdByTel(String tel) {
        return userMapper.queryIdByTel(tel);
    }


    /**
     * 通过用户id获取订单图书详情
     * @param userId
     * @return
     */
    @Override
    public List<ShoppingCar> queryShopByUserId(int userId) {
        return userMapper.queryShopByUserId(userId);
    }


    /**
     * 添加收藏
     * @param userId
     * @param bookId
     * @return
     */
    @Override
    public int addFavorite(int userId, int bookId) {
        List<Integer> bookIds =  userMapper.queryFavorite(userId);
        if(bookIds.contains(bookId)){
            return -1;
        }else {
            return userMapper.addFavorite(userId,bookId);
        }
    }

    /**
     * 删除收藏
     * @param userId
     * @param bookId
     * @return
     */
    @Override
    public int delFavorite(int userId, int bookId) {
        return userMapper.delFavorite(userId,bookId);
    }

    /**
     * 查询收藏
     * @param userId
     * @return
     */
    @Override
    public List<Integer> queryFavorite(int userId) {
        return userMapper.queryFavorite(userId);
    }

    @Override
    public boolean confirmFavorite(int userId, int bookId) {
        return userMapper.confirmFavorite(userId,bookId)>=1?true:false;
    }

    /**
     * 查询所有号码
     * @return
     */
    @Override
    public List<String> queryTel() {
        return userMapper.queryTel();
    }

    /**
     * 删除购物车图书
     * @param userId
     * @param id
     * @return
     */
    @Override
    public int delShopCarBook(int userId, int id) {
        return userMapper.delShopCarBook(userId,id);
    }

    /**
     * 添加购物车
     * @param shoppingCar
     * @return
     */
    @Override
    public int addShoppingCar(ShoppingCar shoppingCar) {
        return userMapper.addShopCar(shoppingCar);
    }

    /**
     * 通过用户id和图书id判断图书是否重复
     * @param userId
     * @param bookId
     * @return
     */
    @Override
    public int queryShopCarByUseridAndBookid(int userId, int bookId) {
        return userMapper.queryShopCarByUseridAndBookid(userId,bookId);
    }

}
