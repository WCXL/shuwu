package com.kgc.controller;

import com.kgc.entity.ShippingAddress;
import com.kgc.service.ShippingAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("address")
@ResponseBody
public class ShippingAddressController {

    @Autowired
    private ShippingAddressService shippingAddressService;

    @RequestMapping("queryCountByUserId")
    public boolean queryCountByUserId(Integer userid){
        int count = shippingAddressService.queryCountByUserId(userid);
        if (count>0){
            return true;
        }else{
            return false;
        }
    }

    @RequestMapping("queryAddressById")
    public List<ShippingAddress> queryAddressById(Integer userid){
        return shippingAddressService.queryAddressById(userid);
    }

    @RequestMapping("addAddress")
    public boolean addAddress(ShippingAddress address){
        return shippingAddressService.addAddress(address) == 1 ? true : false;
    }

    @RequestMapping("delAddressById")
    public boolean delAddressById(int id){
        if (shippingAddressService.delAddressById(id)>0){
            return true;
        }else {
            return false;
        }
    }

    @RequestMapping("queryAddressByAddressId")
    public ShippingAddress queryAddressByAddressId(Integer id){
        return shippingAddressService.queryAddressByAddressId(id);
    }

    @RequestMapping("updateAddressById")
    public boolean updateAddressById(ShippingAddress address){
        if (shippingAddressService.updateAddressById(address)>0){
            return true;
        }else {
            return false;
        }
    }
}
