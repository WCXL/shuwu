<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <style>
        *{
            padding: 0px;
            margin: 0px;
            font-size: 12px;
            color: #666666;
        }

        .index_nav{
            padding:10px 0px 0px 20px;
            height: 50px;
        }

    </style>
</head>
<body>
<div  style=" width: 700px; ">
    <div class="index_nav">个人中心-首页</div>
    <div  id="pc_index" style="width: 130px; margin: 0px auto;">
        <img src="${user.head}" width="130px" height="130px" style="border-radius: 50%;"/>
        <ul class="index_detail" style="list-style: none;padding-left: 30px;">
            <li>你好:${user.name}</li>
            <li><a href="pc_detail.jsp">详细信息</a></li>
            <li><a href="pc_up.jsp">修改密码</a></li>
        </ul>
    </div>

</div>
</body>
</html>
