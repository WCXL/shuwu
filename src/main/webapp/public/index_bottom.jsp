
<%@ page contentType="text/html;charset=UTF-8" language="java" %>





<div class="container">
    <div class="row">
        <div class="col-md-10 di">
            <!-- 选项卡组件（菜单项nav-pills）-->
            <ul class="nav nav-tabs" role="tablist">
                <!-- 默认标签 -->
                <li role="presentation" class="active"><a href="#home" role="tab" data-toggle="tab">合作伙伴</a></li>
                <li role="presentation"><a href="#profile" role="tab" data-toggle="tab">友情链接</a></li>
                <li role="presentation"><a href="#messages" role="tab" data-toggle="tab">推荐专题</a></li>

            </ul>

            <!-- 选项卡内容 -->
            <div class="tab-content di_lj">
                <!-- 默认显示内容 -->
                <div role="tabpanel" class="tab-pane active" id="home">
                    <p class="content">
                        <a href="#">搜狐历史</a>
                        <a href="#">中华网文化</a>
                        <a href="#">西陆军事网</a>
                        <a href="#">新东方在线</a>
                        <a href="#">考研帮</a>
                        <a href="#">半壁江中文网</a>
                        <a href="#">天眼查</a>
                        <a href="#">浙江古籍出版社</a>
                        <a href="#">途牛旅游网</a>
                        <a href="#">网络教育</a>
                        <a href="#">荣宝斋</a>
                        <a href="#">我图网</a>
                        <a href="#">沪江英语</a>
                        <a href="#">有声小说</a>
                        <a href="#">每日上书</a>
                    </p>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                    <p class="content">
                        <a href="#">拍卖网站</a>
                        <a href="#">什么值得买</a>
                        <a href="#">军事读书</a>
                        <a href="#">中国会计网</a>
                        <a href="#">公务员考试网</a>
                        <a href="#">可可英语</a>
                        <a href="#">网址大全</a>
                        <a href="#">网址导航</a>
                        <a href="#">中国图书网</a>
                        <a href="#">招聘网</a>
                        <a href="#">加盟</a>
                        <a href="#">千里马招投标</a>
                        <a href="#">猪八戒网</a>
                        <a href="#">跨境电商</a>
                        <a href="#">车讯网</a>
                        <a href="#">亿邦动力网</a>
                        <a href="#">京东营销360</a>
                        <a href="#">返利网</a>
                        <a href="#">959品牌商机网</a>
                        <a href="#">股票</a>
                        <a href="#">绿野网</a>
                        <a href="#">wed114结婚网</a>
                        <a href="#">钻石小鸟</a>
                        <a href="#">智房旅游地产</a>
                        <a href="#">建筑</a>
                        <a href="#">安卓市场</a>
                        <a href="#">北京写字楼网</a>
                    </p>
                </div>
                <div role="tabpanel" class="tab-pane" id="messages">
                    <p class="content">
                        <a href="#">图书大全</a>
                        <a href="#">法律出版社</a>
                        <a href="#">出版社大全</a>
                        <a href="#">科学出版社</a>
                        <a href="#">作家大全</a>
                        <a href="#">人民出版社</a>
                        <a href="#">清华大学出版社</a>
                        <a href="#">中国标准出版社</a>
                        <a href="#">电子工业出版社</a>
                        <a href="#">化学工业出版社</a>
                        <a href="#">北京大学出版社</a>
                        <a href="#">人民卫生出版社</a>
                        <a href="#">商务印书馆</a>
                        <a href="#">中国铁道出版社</a>
                        <a href="#">中国电力出版社</a>
                        <a href="#">中华书局</a>
                        <a href="#">高等教育出版社</a>
                        <a href="#">老舍</a>
                        <a href="#">鲁迅</a>
                        <a href="#">莫言</a>
                        <a href="#">海岩</a>
                    </p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div style="height:2px;width:940px;border-top:1px solid #ccc;float:left;margin-top:15px;"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4 xx1">
            <p>© 2002-2019 PiaoXiang.com 飘香书屋 版权所有</p>
        </div>
        <div class="col-md-11 xx">
            <p>京ICP证041501号 |  京公网安备 11010502035846号 | 出版物经营许可证 | 企业营业执照 | 违法和不良信息举报电话：010-64755951， 举报邮箱 shenhe@PiaoXiang.com</p>
        </div>
    </div>
</div>

