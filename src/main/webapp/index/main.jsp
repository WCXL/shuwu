<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="../img/favicon.ico" mce_href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/book.css" />
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <link rel="stylesheet" href="../css/public_top.css" />
    <link rel="stylesheet" href="../css/public_bottom.css" />
    <script type="text/javascript">
        $(function () {
                $.getJSON("../book/selectAll",{"pn":1,"ps":16},function (data) {
                    var obj = eval(data);
                    var index = 0;
                    var str="";
                    $(obj.list).each(function () {
                        if(index%4==0){
                            str="<div class='col-md-3 book_lb'><ul>";
                        }
                        str +="<li><a href=\"bookInfo.jsp?id="+this.id+"&number="+this.number+"\"><img src="+this.picture+" /></a></li><li class=\"jj\"><a href=\"bookInfo.jsp?id="+this.id+"\">"+this.name+"</a></li><span>"+this.author+" 著</span> <p>￥"+this.price+"&nbsp;</p>";

                        if((index+1)%4==0){
                            str+="</ul></div>";
                            $("#tbody").append(str)


                        }
                            index+=1;

                    })

                })

            })
        $(function () {
            $.getJSON("../book/queryBookType",function (data) {
                var str = "";
                $(".dh").empty();
                $(data).each(function(){
                    str+="<li><a href=/piaoxiang/book/queryByType?bookType="+this.id+"&pageNum=1>"+this.type+"</a></li>";
                })
                $(".dh").append(str);
            })
        })
       $(function () {
           $.getJSON("../book/queryBookType",function (data) {
               var  str="";
               $(".tj").empty();
               $(data).each(function () {
                   str+="好书推荐/<a href=\"/piaoxiang/book/queryByType?pageNum=1&bookType=0\" class=\"gd\">更多</a>"
                   return false;
               })
               $(".tj").append(str);
           })
       })
    </script>
    <title>飘香书屋</title>
</head>

<body>
<%@ include file="../public/public_top.jsp"%>
<div class="navbar navbar-default">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3" >
                <ul class="nav navbar-nav dh">
                    <li><a href="#">文学类</a></li>
                    <li><a href="#">历史类</a></li>
                    <li><a href="#">艺术类</a></li>
                    <li><a href="#">法律类</a></li>
                    <li><a href="#">军事类</a></li>
                    <li><a href="#">童书类</a></li>
                    <li><a href="#">体育类</a></li>
                    <li><a href="#">小说</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!--
    轮播图
-->
<div id="div1">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="4000">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="../img/011.jpg" >

            </div>
            <div class="item">
                <img src="../img/012.jpg" >

            </div>
            <div class="item">
                <img src="../img/013.jpg" >

            </div>
            <div class="item">
                <img src="../img/014.jpg" >

            </div>
            <div class="item">
                <img src="../img/015.jpg" >

            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" id="aaron1"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" id="aaron2"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!--
        作者：offline
        时间：2019-10-16
        描述：图书展示部分
    -->
    <div class="container">
        <div  class="row">
            <div class="col-md-9 books">
                <p class="tj">好书推荐/<a href="#" class="gd">更多</a>
                </p>
                <div style="height:2px;width:870px;border-top:1px solid #ccc;float:left;margin:15px 0px;"></div>

                <div id="tbody" style="margin-left: 70px">

                </div>
            </div>
        </div>
    </div>
 <%@ include file="../public/index_bottom.jsp"%>
</div>
            <script type="text/javascript" src="../js/jquery-1.12.4.js" ></script>
            <script type="text/javascript" src="../js/bootstrap.js" ></script>
            <script>
                $('li.dropdown').mouseover(function() {
                    $(this).addClass('open');
                })
                    .mouseout(function() {
                        $(this).removeClass('open');
                    });
            </script>
</body>
</html>
