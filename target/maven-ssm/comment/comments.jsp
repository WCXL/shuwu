<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>评论</title>
    <style>
        body {
            margin: 0;
            padding: 0;
            border: none;
            outline: none;
        }

        ul {
            list-style: none;
        }

        a {
            text-decoration: none;
        }

        #pn {
            width: 900px;
            height: auto;
            background: #fff;
            margin: 0 300px;
            padding: 20px;
        }

        .list {
            padding: 20px 0;
            position: relative;

        }

        .comment {
            width: 780px;
            padding: 10px 0;
            border-top: 1px solid #eee;
        }

        .comment-right {
            float: left;
            width: 700px;
        }

        .comment-text {
            line-height: 18px;

        }

        .comment-text span {
            color: #eb7350;
        }

        .comment-date {
            font-size: 12px;
            position: absolute;
            margin-left: 670px;
            bottom: 0;
            color: #ccc;
        }

        .comment-dele {
            display: block;
            position: absolute;
            right: 0;
            bottom: 0;
            margin-left: 30px;
        }

        .comment-dele a:hover {
            cursor: pointer;
            color: red;
        }

        .comment_top {
            margin-top: 20px;
            font-size: 30px;
            color: black;
            margin-left: 300px;
        }
    </style>
    <script type="text/javascript">
        //查询评论
        $(function () {
            var bookid = getUrlParam("id");

            $.getJSON("../comment/query", {"bookid": bookid}, function (data) {
                if (data != "") {
                    var str = "";
                    $("#pn").empty();

                    $(data).each(function () {
                        var datetime = this.time.substring(0, this.time.length - 2);
                        str += "<li class=\"list\">\n" +
                            "        <div class=\"comment\">\n" +
                            "            <div class=\"comment-right\">\n" +
                            "                <div class=\"comment-text\"><span class=\"user\">" + this.username + " ：</span>" + this.content + "</div>\n" +
                            "                <div class=\"comment-date\">" + datetime + "</div>\n" +
                            "                <div class=\"comment-dele\">\n" +
                            "<input type='hidden' value='" + this.username + "'>" +
                            "                    <a class='del'>删除<input type='hidden' value='" + this.id + "' ></a>\n" +
                            "                </div>\n" +
                            "            </div>\n" +
                            "        </div>\n" +
                            "    </li>"
                    })
                    $("#pn").append(str);
                } else {
                    var str = "  <div class=\"comment-text\">暂无评论！</div>"

                    $(".comment-right").append(str);
                }
            })
            //删除评论
            $("#pn").on("click", ".del", function () {
                var id = $(this).find("input").val();
                var username = $("#username").val();
                var users = $(this).prev().val();
                if (username == "") {
                    window.location.href = '../login_register/login_register.jsp';
                    alert("登录后才可以删除评论！")
                } else if (username != users) {
                    alert("只可以删除自己的评论！")
                } else {
                    if (confirm("确认删除吗？")) {
                        $.getJSON("../comment/del", {"id": id}, function (data) {
                            if (data) {
                                alert("删除成功！")
                                window.location.reload();
                            } else {
                                alert("删除失败！")
                            }

                        })
                    }
                }
            })

        })


    </script>

</head>
<body>

<p class="comment_top">评论</p>

<ul id="pn">
    <li class="list">
        <div class="comment">
            <div class="comment-right">

            </div>
        </div>
    </li>

</ul>
</body>
</html>
