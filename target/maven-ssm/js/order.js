/*图书id*/
var id = getUrlParam("id");
/*购买图书数量*/
var number = getUrlParam("number");
/*图书库存数量*/
var bookNumber = getUrlParam("bookNumber");
/*用户id*/
var userid = 0;
$(function(){
    userid = $("#userid").val();
/*输入框不符合规则的友好提示*/
    $("input[name=address]").blur(function(){
        if($("input[name='address']").val() == ""){
            $("input[name='address']+span").html("<h5 class='triangle'></h5>请输入详细地址");
        }else{
            $("input[name='address']+span").html("");
        }
    })
    $("input[name=username]").blur(function(){
        if($("input[name='username']").val() == ""){
            $("input[name='username']+span").html("<h5 class='triangle'></h5>请输入姓名");
        }else{
            $("input[name='username']+span").html("");
        }
    })
    $("input[name=phone]").blur(function(){
        if($("input[name='phone']").val() == ""){
            $("input[name='phone']+span").html("<h5 class='triangle'></h5>请输入手机号");
        }else if ($("input[name='phone']").val().length != 11){
            $("input[name='phone']+span").html("<h5 class='triangle'></h5>请输入不低于11位");
        }else{
            $("input[name='phone']+span").html("");
        }
    })
/*保存地址*/
    $("#addAddress").click(function () {
        var address = $("#distpicker option:selected").text()+"  "+$(".zhu_ti input[name=address]").val();
        var postCode = $(".zhu_ti input[name=youZhen]").val();
        var name = $(".zhu_ti input[name=username]").val();
        var phone = $(".zhu_ti input[name=phone]").val();
        if (postCode.length != 6){
            alert("请输入正确的邮编!")
        }else if (phone.length != 11){
            alert("请输入正确的手机号")
        }else {
            $.getJSON("../address/addAddress",{"userid":userid,"address":address,"postCode":postCode,"name":name,"phone":phone},function (data) {
                if (data){
                    alert("添加地址成功！")
                    window.location.reload();
                }else{
                    alert("添加地址失败!")
                }
            })
        }
    })
/*查询该用户id下有没有地址*/
    $.getJSON("../address/queryCountByUserId",{"userid":userid},function (data) {
        if (data){
            /*有地址则拼接该用户下的所有地址*/
            $.getJSON("../address/queryAddressById",{"userid":userid},function (data1) {
                var str ="<div class='zhu_ti1'>\n" +
                    "<h4>收货地址</h4>";
                $(data1).each(function () {
                    str+=
                        "<div class='zhu_ti1_background'>\n" +
                        "<div class='zhu_ti1_div1'>\n" +
                        "<input type='radio' name='address_chose' value='"+this.id+"'/>\n" +
                        "<span>"+
                        this.name+"("+this.phone+")"+
                        "</span>\n" +
                        "</div>\n" +
                        "<div class='zhu_ti1_div2'>"+
                        this.address+
                        "</div>\n" +
                        "</div>\n"
                })
                str +=  "</div>";
                $("#addressDiv").append(str);
            })
        }else{
            /*没有则展示新增地址的列表*/
            $(".zhu_ti").css("display","block");
            $(".zhu_ti1").css("display","none");

        }


    });
/*省市县三级联动*/
    $("#distpicker").distpicker();
/*通过图书id查询该图书*/
    $.getJSON("../book/queryBookById", {"id": id}, function(data){
        $(".commodity_div1 img").attr("src",data.picture);
        $(".commodity_div2").html(data.name);
        $(".commodity_div4").html(data.price);
        $(".commodity_div5").html("×"+number+"件");
        $(".commodity_div6").html(data.price*number);
        var price_sum = (data.price*number)+16;
        $(".span_price").html("￥"+price_sum);
    });
    /*支付传值*/
    $(".order").click(function () {
        if ($(".zhu_ti1 input[type=radio]:checked").val() != null){
            var in_phone = "";
            var in_name = "";
            var addressid = $(".zhu_ti1 input[type=radio]:checked").val()*1;
            $.getJSON("../address/queryAddressByAddressId",{"id":addressid},function (data) {
                in_phone = data.phone;
                in_name = data.name;
                var in_money = $(".span_price").text().substring(1);
                window.location.href = "../Alipay/to_alipay?in_name=" + in_name
                    + "&&in_phone=" + in_phone + "&&in_money=" + in_money + "&&userid="+userid
                    + "&&bookid=" + id + "&&bookNumber=" + number + "&&addressid=" + addressid+"&&numberAll="+bookNumber;
            })

        }else{
            alert("请选择收货地址！")
        }

    })
})
/*从地址栏获取参数*/
function getUrlParam(key) {
    // 获取参数
    var url = window.location.search;
    // 正则筛选地址栏
    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
    // 匹配目标参数
    var result = url.substr(1).match(reg);
    //返回参数值
    return result ? decodeURIComponent(result[2]) : null;
}
/*邮编查询地址跳转*/
function you_bian() {
    window.open("http://cpdc.chinapost.com.cn/web/");
}


