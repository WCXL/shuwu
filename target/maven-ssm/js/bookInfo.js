/*获取地址栏id*/
var id = getUrlParam("id");
var number = getUrlParam("number");
var unitPrice = 0;
function getUrlParam(key) {
    // 获取参数
    var url = window.location.search;
    // 正则筛选地址栏
    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
    // 匹配目标参数
    var result = url.substr(1).match(reg);
    //返回参数值
    return result ? decodeURIComponent(result[2]) : null;
}
/*ajax通过id获取图书详细*/
$.getJSON("../book/queryBookById", {"id": id}, function(data){
    $(".pic img").attr("src",data.picture);
    $(".title h4").html(data.name);
    $(".author").html(data.author);
    $(".publish").html(data.publish);
    $(".price").html(data.price);
    unitPrice = data.price;
    $("#book_time").html(data.publishDate);
    $(".book_intro_Info").html(data.content);
    if (data.number==0){
        $("#book_num").html("已售空");
        $(".btn_buy_1,.btn_buy_2,.shopNumber input[type=button]").attr("disabled","true");
        $(".shopNumber input[type=text]").attr("value",0);
    }
    if (data.number==1){
        $("#book_num").html("只剩一件，预购从速");
    }
    if (data.number>1){
        $("#book_num").html(data.number+"本");
    }
});
/*立即购买按钮判断*/
function shoping(){
    var userId = document.getElementById('userId').value;
    var shopnumber = document.getElementById("amount").value;
    if ( userId != "" ){
        window.open("order.jsp?id="+id+"&&number="+shopnumber+"&&bookNumber="+number+"");
    }else{
        $("#loginmodal").modal("show");
    }
}
/*加入购物车按钮*/
function addShoppingCar(){
    var userId = document.getElementById('userId').value;
    var shopnumber = document.getElementById("amount").value;
    $.getJSON("../user/addShoppingCar",{"book_id":id,"user_id":userId,"book_num":shopnumber},function (data) {
        if (data){
            alert("加入购物车成功!")
        }else {
            alert("不能重复加入购物车!")
        }
    })
}


$(function () {
    $(".shopNumber input[name=minus]").click(function () {
        if(($(".shopNumber input[type=text]").val()*1) <= 1){
            $(".shopNumber input[type=text]").val(1);
            return;
        }else{
            $(".shopNumber input[type=text]").val(($(".shopNumber input[type=text]").val()*1)-1);
            $("#bookPrice").html((unitPrice*$(".shopNumber input[type=text]").val()).toFixed(2));
        }
    })
    $(".shopNumber input[name=plus]").click(function () {
        if(($(".shopNumber input[type=text]").val()*1) >= number){
            $(".shopNumber input[type=text]").val(number);
            return;
        }else{
            $(".shopNumber input[type=text]").val(($(".shopNumber input[type=text]").val()*1)+1);
            $("#bookPrice").html((unitPrice*$(".shopNumber input[type=text]").val()).toFixed(2));
        }
    })
})
