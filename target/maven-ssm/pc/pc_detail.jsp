<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <style>
        * {
            padding: 0px;
            margin: 0px;
            font-size: 12px;
            color: #666666;

        }

        .detail {
            width: 700px;
            padding: 10px;
        }

        .detail a {
            text-decoration: none;
        }

        .detail a:hover {
            color: orangered;
        }

        .detail_title {
            height: 50px;
            border-bottom: 1.5px solid gainsboro;
            margin-bottom: 20px;
        }

        .detail_title span {
            font-size: 20px;
        }

        .detail_head {
            margin-left: 120px;
            width: 100px;
            text-align: center;
            float: left;
        }

        .detail_head img {

            width: 100px;
            height: 100px;
            border-radius: 50%;
        }

        .detail_body {
            width: 350px;
            float: left;
            margin-left: 100px;
        }

        .detail_body_ul {
            list-style: none;
        }

        .detail_body_ul li {
            height: 30px;
            border-bottom: 1px solid gainsboro;
            line-height: 30px;
        }

        .detail_update {
            font-size: 10px;
            float: right;
            margin-right: 20px;
        }

        .detail_nav {
            padding: 10px 0px 0px 20px;
            height: 10px;
        }

        #user_submit {

            float: right;

        }
        .detail_body_ul input{
            width: 120px;
            height: 20px;
            border: 1px solid gray;
        }

        #user_submit button {
            width: 60px;
            height: 20px;
            line-height: 20px;
            text-align: center;
            border: none;
            color: white;
            background-color: gray;
            margin: 10px 20px 0px 0px;
        }

        input[type="radio"]{
            width: 12px;
            height: 12px;
        }

        .radiospan{
            display: inline-block;
            width: 20px;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }
        input[type="number"]{
            -moz-appearance: textfield;
        }
    </style>

    <script>
        $(function () {
            $("#modify").click(function () {
                $.post("../user/queryUserById", {"id":${user.id}}, function (data) {


                    var myDate = new Date;
                    var yy =myDate.getFullYear();
                    var mm = ""
                    if(myDate.getMonth()<9){
                       mm ="0"+( myDate.getMonth()+1)
                    }else {
                        mm=myDate.getMonth()+1
                    }
                    var dd= ""
                    if(myDate.getDate()<10){
                        dd ="0"+myDate.getDate();
                    }else {
                        dd = myDate.getDate();
                    }
                    var now = yy+"-"+mm+"-"+dd;

                    console.log(now)

                    $("#name").html('<input type="text" name="name" value="'+data.name+'">');
                    if('男' == data.sex){
                        $("#sex").html('<input type="radio" checked  name="sex" value="男"/>男<span class="radiospan"></span><input  type="radio" name="sex" value="女"/>女');
                    }else if('女' == data.sex){
                        $("#sex").html('<input type="radio"   name="sex" value="男"/>男<span class="radiospan"></span><input  checked type="radio" name="sex" value="女"/>女');
                    }else {
                        $("#sex").html('<input type="radio"   name="sex" value="男"/>男<span class="radiospan"></span><input   type="radio" name="sex" value="女"/>女');

                    }
                    $("#age").html('<input type="number" max="120" min="0" name="age" value="'+data.age+'">');
                    $("#birthday").html('<input type="date" max="'+now+'"   min="1990-01-01" name="birthday" value="'+data.birthday+'">');
                    $("#user_submit button").removeAttr("hidden");


                }, "json")
            })

            $("#user_submit button").click(complete);


            function complete() {

                $.post("../user/completeInfo", $("form").serialize(), function (data) {
                    if($("#birthday").children("span").val()!= undefined||($("#age").children("span").val()!= undefined)){
                        return
                    }
                    if (data) {
                        alert("修改成功！");
                        $.post("../user/queryUserById", {"id":${user.id}}, function (data) {
                            $("#name").html(data.name);
                            $("#sex").html(data.sex);
                            $("#age").html(data.age);
                            $("#birthday").html(data.birthday);
                            $("#user_submit button").attr("hidden", "hidden")
                        }, "json")

                    } else {
                        alert("修改失败！");
                    }
                }, "json")
            }

            $("#birthday").on("change","input",function () {
                rem($("#birthday"));
                var myDate = new Date;
                var year = myDate.getFullYear(); //获取当前年
                var mon = myDate.getMonth() + 1; //获取当前月
                var date = myDate.getDate(); //获取当前日

                var birth = $("#birthday input").val();
                var  byear = birth.split("-")[0];
                var bmon = birth.split("-")[1];
                var bdate = birth.split("-")[2];
                if(byear>year||(byear==year&&bmon>mon)||(byear==year&&bmon==mon&&bdate>date)){

                    add($("#birthday"),"日期不能为未来！")
                    return
                }

                if(byear < 1990) {
                    add($("#birthday"),"日期不规范！")
                }
                $("#age input").val(year-byear+1)
            })

            $("#age").on("change","input",function () {

                rem($("#age"))
                var age = $("#age input").val();


                if(age<=0||age>120||isNaN(age)){
                    add($("#age"),"年龄不合法！")
                    return
                }


                var myDate = new Date;
                var year = myDate.getFullYear(); //获取当前年
                var birth = $("#birthday input").val();
                var bmon = birth.split("-")[1];
                var bdate = birth.split("-")[2];

                $("#birthday input").val(year-age+"-"+bmon+"-"+bdate);
            })

            function add(obj,str) {
                obj.append("<span style='color: darkred'>"+str+"</span>")
            }
            function rem(obj){
                obj.children("span").remove();
            }
        })



    </script>
</head>
<body style="background-color: #F5F5F4">
<div class="detail_nav"> 个人中心>个人信息</div>
<div class="detail">

</div><div class="detail_title">
</div>
<div>
    <div class="detail_head">
        <img src="${user.head}"/>
        <a href="#" data-target="#modal" data-toggle="modal">修改头像</a>
    </div>
    <div class="detail_body">
        <form>
            <input type="hidden" name="id" value="${user.id}">
            <ul class="detail_body_ul">

                <li>
                    <span>基础资料<a class="detail_update" id="modify">编辑</a></span>
                </li>
                <li>
                    昵称：<span id="name">${user.name}</span>
                </li>
                <li>
                    性别：<span id="sex">${user.sex}</span>
                </li>
                <li>
                    年龄：<span id="age">${user.age}</span>
                </li>
                <li>
                    生日：<span id="birthday">${user.birthday}</span>
                </li>


            </ul>

            <span id="user_submit"><button type="button" hidden="hidden">确认</button></span>
        </form>
    </div>
</div>
<jsp:include page="../public/uploadheadmodal.jsp"></jsp:include>

</body>
</html>
