<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="../css/bootstrap.css"/>
    <link rel="stylesheet" href="../css/orderlist.css"/>
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <style>
        .orderbottom div{
            float: left;
        }
    </style>


</head>

<body style="background-color: #F5F5F5">
<div class="order_nav"> 个人中心>我的订单</div>
<div class="body" id="or_body"  style="width: 700px;margin: 0px auto;">

    <div>
        <ul class="breadcrumb" id="otype">
            <li>
                <a id="query0">全部订单</a>
            </li>
            <li>
                <a id="query1">已下单</a>
            </li>
            <li >
                <a id="query2">已支付</a>
            </li>
            <li >
                <a id="query3">已确认</a>
            </li>
        </ul>
    </div>
    <div id="or_body2">

    </div>

</div>

<script>
    $(function () {

        $.post("../order/queryOrder",{"userId":"${user.id}"},function (data) {
            $("#query0").css("color","darkred")
            if(data == null || data == ""){
                $("#or_body").append("<span class='noor'>您还未购买任何东西，<a href='javascript:window.parent.location.href = \"../index/main.jsp\"'>" +
                    "点此前往首页</a></span>")
                $("#otype").css("display","none")
            }else {
                $(data).each(function () {
                    var order = this;
                    $.getJSON("../book/queryBookById", {"id": this.bookId}, function (book) {
                        var str = " <div class=\"order-only\">\n" +
                            "        <h4>" + order.type + "</h4>\n" +
                            "        <div class=\"header\">\n" +
                            "            <div class=\"header-detail\">" + order.address + "</div>\n" +
                            "            <div class=\"header-price\">订单金额：<span>" + book.price * order.bookNumber + "</span>元</div>\n" +
                            "        </div>\n" +
                            "        <div class=\"clearfix\"></div>\n" +
                            "        <div class=\"orderbottom\" style=\"border-top: 1px solid gainsboro; margin-top: 20px; padding-top: 20px;\">\n" +
                            "            <div style=\"width: 100px\"><img src=\"" + book.picture + "\" ></div>\n" +
                            "            <div style=\"width: 460px\">\n" +
                            "                <ul>\n" +
                            "                    <li>" + book.name + "</li>\n" +
                            "                    <li>￥" + book.price + "&times;" + order.bookNumber + "</li>\n" +
                            "                </ul>\n" +
                            "            </div>\n" +
                            "            <div style=\"width: 80px\">\n" +
                            "<input type='hidden' class='orderNumber' value='" + order.orderNumber + "'>" +
                            "<input type='hidden' class='bookNumber' value='" + order.bookNumber + "'>" +
                            "<input type='hidden' class='bookid' value='" + book.id + "'>" +
                            "<input type='hidden' class='bookNumberAll' value='" + book.number + "'>" +
                            "<input type='hidden' class='orderId' value='" + order.id + "'>";
                        order.type == "已下单" ?
                            str += "<button  class=\"order-btn pay_order\" data-toggle=\"button\">付款</button>\n"
                            : (order.type == "已支付") ?
                            str += "<button  class=\"order-btn affirm_order\" data-toggle=\"button\">确认订单</button>\n"
                            : order.type == "已确认" ?
                                str += "<button disabled class=\"order-btn\" data-toggle=\"button\">已确认</button>\n"
                                : str += "<button disabled class=\"order-btn\" data-toggle=\"button\">已确认</button>\n";

                        str += "<button class=\"order-btn shopInfo\" data-toggle=\"button\">商品详情</button>\n" +
                            "            </div>\n" +
                            "        </div>\n" +
                            "    </div>";
                        $("#or_body2").append(str)
                    })

                })
            }

        },"json")

        $("#or_body").on("click",".pay_order",function () {
            var orderMoney = $(this).parents(".order-only").find(".header-price").find("span").html();
            var bookid = $(this).parents(".order-only").find(".bookid").val();
            var bookNumber =$(this).parents(".order-only").find(".bookNumber").val();
            var numberAll =$(this).parents(".order-only").find(".bookNumberAll").val();
            var orderNumber =$(this).parents(".order-only").find(".orderNumber").val();
            window.parent.location.href="../Alipay/pay2?orderNumber=" + orderNumber +"&&in_money=" + orderMoney+
                "&&bookid="+bookid+"&&bookNumber="+bookNumber+"&&numberAll="+numberAll;
        })
        $("#or_body").on("click",".affirm_order",function () {
            var orderNumber =$(this).parents(".order-only").find(".orderNumber").val();
            var flag = confirm("是否确认收货？");
            if (flag){
                $.getJSON("../order/updataOrderType",{"orderType":3,"orderNumber":orderNumber},function (data) {
                    if (data){
                        alert("确认收货成功!");
                        window.parent.location.href="/piaoxiang/pc/pc_top.jsp?ifurl=orderlist";
                    }else {
                        alert("确认收货失败，请联系客服进行查询!");
                    }
                })
            }
        })
        $("#or_body").on("click",".shopInfo",function () {
            var bookid = $(this).parents(".order-only").find(".bookid").val();
            var numberAll =$(this).parents(".order-only").find(".bookNumberAll").val();
            window.parent.location.href="/piaoxiang/index/bookInfo.jsp?id="+bookid+"&number="+numberAll;
        })


        $("#query0").click(function () {
            $("a").css("color","#337AB7")
            $(this).css("color","darkred")
            queryByType(0)
    });
        $("#query1").click(function () {
            $("a").css("color","#337AB7")
            $(this).css("color","darkred")
            queryByType(1)
        });
        $("#query2").click(function () {
            $("a").css("color","#337AB7")
            $(this).css("color","darkred")
            queryByType(2)
        });
        $("#query3").click(function () {
            $("a").css("color","#337AB7")
            $(this).css("color","darkred")
            queryByType(3)
        });

        function queryByType(type){

            $.getJSON("../order/queryOrderByType",{"userId":"${user.id}","orderType":type},function (data) {
                $("#or_body2").empty();
                $(data).each(function () {
                    var order = this;
                    $.getJSON("../book/queryBookById", {"id": this.bookId}, function (book) {
                        var str = " <div class=\"order-only\">\n" +
                            "        <h4>" + order.type + "</h4>\n" +
                            "        <div class=\"header\">\n" +
                            "            <div class=\"header-detail\">" + order.address + "</div>\n" +
                            "            <div class=\"header-price\">订单金额：<span>" + book.price * order.bookNumber + "</span>元</div>\n" +
                            "        </div>\n" +
                            "        <div class=\"clearfix\"></div>\n" +
                            "        <div class=\"orderbottom\" style=\"border-top: 1px solid gainsboro; margin-top: 20px; padding-top: 20px;\">\n" +
                            "            <div style=\"width: 100px\"><img src=\"" + book.picture + "\" ></div>\n" +
                            "            <div style=\"width: 460px\">\n" +
                            "                <ul>\n" +
                            "                    <li>" + book.name + "</li>\n" +
                            "                    <li>￥" + book.price + "&times;" + order.bookNumber + "</li>\n" +
                            "                </ul>\n" +
                            "            </div>\n" +
                            "            <div style=\"width: 80px\">\n" +
                            "<input type='hidden' class='orderNumber' value='" + order.orderNumber + "'>" +
                            "<input type='hidden' class='bookNumber' value='" + order.bookNumber + "'>" +
                            "<input type='hidden' class='bookid' value='" + book.id + "'>" +
                            "<input type='hidden' class='bookNumberAll' value='" + book.number + "'>" +
                            "<input type='hidden' class='orderId' value='" + order.id + "'>";
                        order.type == "已下单" ?
                            str += "<button  class=\"order-btn pay_order\" data-toggle=\"button\">付款</button>\n"
                            : (order.type == "已支付") ?
                            str += "<button  class=\"order-btn affirm_order\" data-toggle=\"button\">确认订单</button>\n"
                            : order.type == "已确认" ?
                                str += "<button disabled class=\"order-btn\" data-toggle=\"button\">已确认</button>\n"
                                : str += "<button disabled class=\"order-btn\" data-toggle=\"button\">已确认</button>\n";

                        str += "<button class=\"order-btn shopInfo\" data-toggle=\"button\">商品详情</button>\n" +
                            "            </div>\n" +
                            "        </div>\n" +
                            "    </div>";


                        $("#or_body2").append(str)
                    })

                })
            })
        }


    })



</script>

</body>

</html>
