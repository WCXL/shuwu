<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">

    <title>注册页</title>
    <link rel="stylesheet" href="../css/bootstrap.css"/>
    <link rel="stylesheet" href="../css/register.css"/>
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <style>

    </style>
    <script>
        $(function () {

            var time = 60;

            var flag = false;
            $("#tel").blur(checktel);
            $("#name").blur(checkname);
            $("#password").blur(checkpwd);

            function checkpwd() {
                rem($(this));
                var pwd = $(this).val();
                if (pwd == "") {
                    add($(this), "密码不能为空！");
                    return false;
                }
                if (pwd.length < 6) {
                    add($(this), "密码不能少于6位！");
                    return false;
                }


            }

            function checktel() {
                rem($(this));
                var tel = $(this).val();
                var reg = /^1[35789]\d{9}$/
                if (!reg.test(tel)) {
                    add($(this), "手机号格式不正确！")
                }
            }

            function checkname() {
                rem($(this));
                var name = $(this).val();
                if (name == "") {
                    add($(this), "昵称不能为空！");
                    return false;
                }

            }

            function add(obj, t) {
                rem(obj);
                obj.after("<h5 class='triangle1'>左实心三角</h5><span>" + t + "</span>");
                obj.siblings("span").css({
                    "color": "darkred",
                    "font-size": "10px"
                });
            }

            function rem(obj) {
                obj.siblings("span").remove();
                obj.siblings("h5").remove();
            }

            $("#code-btn").click(function () {
                var reg = /^1[35789]\d{9}$/;

                var phoneNo = $("#tel").val();

                if (phoneNo == "" || phoneNo.length != 11||!reg.test(phoneNo)) {
                    return;
                }

                $.getJSON("../user/isexisttel",{"tel":phoneNo},function (data) {
                    if(data){
                        alert("手机号已注册！")
                        return;
                    }else {
                        $.post("../user/sendSms", {"phoneNo": phoneNo}, function (data) {
                            flag = data;
                            if (data) {
                                alert("验证码发送成功！");
                            } else {
                                alert("验证码发送失败！");
                            }
                        }, "json")
                        sixty();
                    }
                })



            })


            $("#submit").click(function () {


                var pwd = $("#password").val();
                var code = $("#code").val();
                var tel = $("#tel").val();
                var name = $("#name").val();
                if (code == "" || !flag
                    || tel == "" || name == ""
                    || pwd == "") {
                    return;
                }

                if (!$("#check").is(":checked")) {

                    alert("请阅读协议并同意！");
                    return;
                }


                $.getJSON("../user/register", $("form").serialize(), function (data) {
                    if (data == 1) {
                        alert("注册成功！");
                        window.location.href="login_form.jsp";
                    } else if (data == 2) {
                        alert("注册失败！")
                    } else if (data == 3) {
                        add( "验证码错误！")
                    }
                })

            })

            function sixty() {

                if (time == 0) {
                    time = 60;
                    $("#code-btn").html("获取验证码");
                    $("#code-btn").removeAttr("disabled");
                    return;
                } else {
                    time--;
                    $('#code-btn').html("(" + time + "秒" + ")重发");
                }
                setTimeout(function () {
                    sixty();
                }, 1000);

                $("#code-btn").attr("disabled", "disabled");
            }

        })

    </script>
</head>

<body>
<div class="register-form">
    <div class="from-head">
        <div class="register_title">
            <h2>注册</h2>
        </div>
        <div class="clearfix"></div>
        <div class="login">已有账号？
            <a href="login_form.jsp">请登录</a>
        </div>
    </div>
    <div class="clearfix"></div>
    <form class="form-inline" role="form">
        <div><input id="tel" type="tel" name="tel"  maxlength="11" placeholder="手机号"/></div>
        <div><input class="input-code" id="code" type="text" maxlength="4" name="usercode" placeholder="验证码"/>
            <button type="button" id="code-btn" class="code-btn">获取验证码</button>
        </div>
        <div><input id="name" type="text" name="name" maxlength="12" placeholder="用户名"/></div>

        <div><input id="password" type="password" name="password" maxlength="12" placeholder="密码"/></div>
        <!--<div style="height: 10px;></div>-->
        <div class="checkbox" style="margin-top: 10px;">
            <input id="check" type="checkbox" style="position:relative;margin-left: 0px"> 我已阅读并接受
            <a href="#" style="font-size: 8px;">《服务协议》</a>和
            <a href="#" style="font-size: 8px;">《账户协议》</a>
        </div>
        <div>
            <button type="button" id="submit">注册</button>
        </div>
    </form>
</div>

</body>

</html>