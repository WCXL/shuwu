<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="../css/updatepassword.css">
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.css" />
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <link rel="stylesheet" href="../css/public_top.css" />
    <link rel="stylesheet" href="../css/public_bottom.css" />
    <script>
        $(function () {
            $("#code-btn").click(function () {
                var tel=$("#tel").val();
                if(tel!=null){
                    $.post("../user/sendSms",{"phoneNo":tel},function (data) {
                        if (data){
                            alert("获取验证码成功！");
                        } else {
                            alert("获取验证码失败！");
                        }
                    },"json")

                }

            })

            $("#next_1").click(function () {

                var newcode=$("#code").val();
                $.post("../user/confirmup",{"usercode":newcode},function (data) {
                    if (data) {
                        $(".up_step1").removeClass("check");
                        $(".step_1").css("display","none");

                        $(".up_step2").addClass("check");
                        $(".step_2").css("display","block");
                    }else {
                        alert("验证码输入错误！");
                    }
                },"json")


            })

            $("#next_2").click(function () {

                var newpassword=$("#newpassword").val();
                var repeatpassword=$("#repeatpassword").val();
                if (newpassword==""){
                    alert("密码不能为空！");

                    $(".up_step2").removeClass("check");
                    $(".step_2").css("display","none");


                    $(".up_step2").removeClass("check");
                    $(".step_2").css("display","none");


                }else if(newpassword.length<6){
                    alert("密码不能小于6位！");

                }
                else if (newpassword!=repeatpassword){
                    alert("两次输入密码不相同！");

                } else {
                    var tel=$("#tel").val();
                    $.post("../user/queryidbytel",{"tel":tel},function (data){
                        var id=$("#userid").val();
                        $.post("../user/updatePassword",{"id":data,"password":newpassword},function (data) {
                            if (data){
                                five();
                                $(".up_step2").removeClass("check");
                                $(".step_2").css("display","none");

                                $(".up_step3").addClass("check")
                                $(".step_3").css("display","block");
                            }
                        },"json")

                    },"json")

                }
            })
            $(".up_step1").bind("click",step1);
            function step1() {
                $(".up_step1").addClass("check");
                $(".step_1").css("display","block");

                $(".up_step2").removeClass("check");
                $(".step_2").css("display","none");
                $(".up_step3").removeClass("check");
                $(".step_3").css("display","none");
            }

            var time = 5;
            function five() {

                if (time == 0) {
                    time = 5;
                    window.parent.location.href="../user/clearlogin";
                    return;
                } else {
                    time--;
                    $('#five').html(time+1);
                }
                setTimeout(function () {
                    five();
                }, 1000);

            }


        })

    </script>
</head>

<body>
<jsp:include page="../public/public_top.jsp"></jsp:include>
<div class="up_body" style="width: 1000px;margin: 100px auto">
    <div class="up_title">
        <div class="up_step1  check">
            <span>1</span>确认手机号
        </div>
        <div class="up_title_wire"></div>
        <div class="up_step2">
            <span>2</span>重置密码
        </div>
        <div class="up_title_wire"></div>
        <div class="up_step3">
            <span>3</span>重置成功
        </div>

    </div>

    <div>
        <div class="step_1">
            <form>

                <div>手机号：<input type="text" name="tel" id="tel" ></div>
                <div><input type="text" name="code" id="code" placeholder="请输入验证码"/>
                    <button type="button" id="code-btn" class="code-btn">获取验证码</button>
                </div>
                <div>
                    <button type="button" id="next_1">下一步</button>
                </div>
            </form>
        </div>
        <div class="step_2">
            <span>新密码：</span><input type="password" id="newpassword" name="newpassword"/>
            <span>确认新密码：</span><input type="password" id="repeatpassword" name="repeatpassword"/>
            <div>
                <button type="button" id="next_2">下一步</button>
            </div>
        </div>
        <div class="step_3" style="margin-left: 200px">
            重置成功！<span id="five">5</span>秒后自动跳转到登录页&nbsp<a href="javascript:window.parent.location.href='../user/clearlogin'">点此手动跳转</a>
        </div>

    </div>
</div>
<jsp:include page="../public/public_bottom.jsp"></jsp:include>
</body>

</html>