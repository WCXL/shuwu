<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="icon" href="../img/favicon.ico" mce_href="../img/favicon.ico" type="image/x-icon">
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/bookShopping.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.css"/>
    <link rel="stylesheet" href="../css/bookShopping.css"/>
    <link rel="stylesheet" href="../css/public_top.css"/>
    <link rel="stylesheet" href="../css/public_bottom.css"/>

    <title>账号登录后的购物车</title>
    <script type="text/javascript">
        <%-- 通过用户id和图书id数据库获取信息 --%>
        $(function () {
                // 未登录时，购物车的页面
                if ("" == "${user.id}") {
                    var str = '<div class="shoppingCar"><img src="../img/shoppingCar.png">对不起，您还未登录，您可以<a href="../login_register/login_register.jsp">立即登录</a></div>'
                    $(".shoppingCarAll").empty();
                    $(".shoppingCarAll").append(str);
                } else {
                    var userId = $("#userId").val();
                    $.getJSON("../user/queryShopByUserId", {"userId": userId}, function (data1) {
                        // 登录时，购物车为空的页面
                        if (data1 == "") {
                            var str = '<div class="shoppingCar"><img src="../img/shoppingCar.png">您的购物车是空的，您可以<a href="../index/main.jsp">点击这里</a>去挑选图书</div>'
                            $(".shoppingCarAll").empty();
                            $(".shoppingCarAll").append(str);
                        }
                        $(data1).each(function () {
                            var oid = this.id;
                            var onum = this.book_num;
                            $.getJSON("../book/queryBookById", {"id": this.book_id}, function (data) {
                                var str = "<div class=\"shoppingCarBody\"><ul><li class=\"check\"><input type=\"checkbox\" class=\"books-check\"></li><li class=\"bookImg\"><a href=\"bookInfo.jsp?id= " +
                                    data.id + "&number=" +
                                    data.number + "\"><img src='" +
                                    data.picture + "'/></a></li><li class=\"bookInfo\"><p class=\"bookName\">书名：" +
                                    data.name + "</p><p class=\"bookAuthor\">作者：" +
                                    data.author + "</p></li><li class=\"bookPrice\" name=\"bookPrice\"><span>¥</span><span class=\"price\">" +
                                    data.price + "</span></li><li class=\"bookSub-Add\"><span type=\"button\" class=\"bookSub\">-</span><input class=\"bookSA\" type=\"text\" value='" +
                                    onum + "'  autocomplete=\"off\"/><input type='hidden' id='count' value='" +
                                    data.number + "'><span type=\"button\" class=\"bookAdd\">+</span></li><li class=\"bookPrices\" id=\"bookPrices\" ><span>¥</span><span class=\"prices\">" +
                                    (data.price * onum).toFixed(2) + "</span></li><li class=\"bookButton\"><button type='button' class='btn1' >加入收藏<input type='hidden' class='s_book' value='" +
                                    data.id + "'></button><button type='button' class='btn2' >删除<input type='hidden' value='" +
                                    oid + "'></button></li></ul></div>";
                                $(".bookInsert").append(str);
                            });
                        });
                    });
                }
                <%-- 通过用户id和图书id删除购物车图书信息 --%>
                $("#bookInsert").on("click", ".btn2", function () {
                    if (confirm("确认删除？")) {
                        var id = $(this).children("input").val();
                        $.getJSON("../user/delShopCarBook", {"userId": userId, "id": id}, function (data) {
                            if (data) {
                                alert("删除成功！")
                                window.location.href = "bookShopping.jsp";
                            } else {
                                alert("删除失败");
                            }
                        });
                    }
                    ;
                });
                <%-- 通过用户id和图书id将购物车图书信息添加到数据库收藏表 --%>
                $("#bookInsert").on("click", ".btn1", function () {
                    var bookId = $(this).children("input").val();
                    // alert(userId)
                    // alert(bookId)
                    $.getJSON("../user/addFavorite", {"userId": userId, "bookId": bookId}, function (data) {
                        // console.log(data)
                        if (data) {
                            alert("已加入收藏！")
                            // window.location.href = "bookShopping.jsp";
                        } else {
                            alert("商品已加入收藏，无法继续添加");
                        }
                    });
                });
                <%-- 通过数量的加减更新购物车数据库图书的数量 --%>
                // 减
                $("#bookInsert").on("click", ".bookSub", function () {
                    var num = $(this).parent().find(".bookSA");
                    var price = parseFloat($(this).parents(".shoppingCarBody").find(".price").text());
                    if (num.val() <= 1) {
                        alert("再减就没有了！")
                        return;
                    }
                    num.val(parseInt(num.val()) - 1);
                    //计算单个商品的总价
                    var prices = price * num.val();
                    $(this).closest(".shoppingCarBody").find(".prices").text(prices.toFixed(2));
                    TotalPrice();
                    // alert("减")
                });
                // 加
                $("#bookInsert").on("click", ".bookAdd", function () {
                    var num = $(this).parent().find(".bookSA");
                    var price = parseFloat($(this).parents(".shoppingCarBody").find(".price").text());
                    //获取数据库这本书的数量
                    var count = $(this).parents(".shoppingCarBody").find("#count").val();
                    if (num.val() >= count) {
                        alert("库存就这么多了，小二正在加紧备货！")
                        return;
                    }
                    num.val(parseInt(num.val()) + 1);
                    //计算单个商品的总价
                    var prices = price * num.val();
                    $(this).closest(".shoppingCarBody").find(".prices").text(prices.toFixed(2));
                    TotalPrice();
                    // alert("加")
                });

                // 单选按钮
                $("#bookInsert").on("click", ".books-check", function () {
                    if ($(this).prop("checked") == true) {
                        console.log($(this).prop("checked") == true)
                        //如果选中的商品等于所有商品
                        if ($(".books-check").length == $(".books-check:checked").length) {
                            //全选按钮被选中
                            $(".check-all").prop('checked', true);
                            console.log($(".books-check").length == $(".books-check:checked").length)
                            TotalPrice();
                        } else {
                            //全选按钮不被选中
                            $(".check-all").prop('checked', false);
                            TotalPrice();
                        }
                    } else {
                        //else全选按钮不被选中
                        $(".check-all").prop('checked', false);
                        TotalPrice();

                    }
                    TotalPrice();
                });

                // 全选按钮
                $(".check-all").click(function () {
                    //如果全选按钮被选中
                    if ($(this).prop("checked") == true) {
                        $(".books-check").prop("checked", true);
                        $(".check-all").prop("checked", true);
                        //所有按钮都被选中
                        TotalPrice();
                    } else {
                        //所有按钮全不选
                        $(".books-check").prop('checked', false);
                        $(".check-all").prop('checked', false);
                        TotalPrice();
                    }

                });

                //计算商品总价和总数量
                function TotalPrice() {
                    //总价
                    var allprice = 0;
                    //商品总数
                    var countNum = 0;
                    //循环整个购物车
                    $(".shoppingCarBody").each(function () {
                        //循环购物车所有单选按钮里面的商品
                        $(this).find(".books-check").each(function () {
                            //如果单选按钮被选中
                            if ($(this).is(":checked")) {
                                //得到商品的数量
                                var num = parseInt($(this).parents(".shoppingCarBody").find(".bookSA").val());
                                //得到被选中商品的价格
                                var oneprice = parseFloat($(this).parents(".shoppingCarBody").find(".prices").text());
                                // 计算全部商品数量
                                countNum += num;
                                //计算所有被选中商品的总价
                                allprice += oneprice;
                                console.log(allprice)
                            }
                            //输出选中的商品的数量
                            $(".bookCount").text(countNum.toFixed(0));
                            //输出全部总价
                            $("#bookMoney").text(allprice.toFixed(2));
                        });
                    });
                    // 结算按钮
                    if ($("#bookMoney").text() != 0.00) {
                        // 当价格等于0.00时，结算不可用
                        $("#s_submit").removeClass("submitDis");
                    } else {
                        // 当价格不等于0.00时，结算可用
                        $("#s_submit").addClass("submitDis");
                    }
                }

                $("#s_submit").click(function () {

                    if($(this).hasClass("submitDis")){
                        return;
                    };
                    var orders ="";
                    $(".books-check:checked").each(function () {
                        orders+=$(this).parents(".shoppingCarBody").find(".s_book").val()+","+$(this).parents(".shoppingCarBody").find(".bookSA").val()+",";

                    })

                    console.log(orders.substring(0, orders.lastIndexOf(',')))

                     $.getJSON("../order/goAccount",{"orders":orders.substring(0, orders.lastIndexOf(','))} ,function () {

                     })
                })
            });


    </script>
</head>

<body>
<jsp:include page="../public/public_top.jsp"></jsp:include>
<input type="hidden" value="${user.id}" id="userId">
<form action="../index/order.jsp">
    <div class="shoppingCarAll">
        <!--头部标题栏-->
        <div class="shoppingCarHead">
            <div class="row">
                <div class="col-md-2 car-title">
                    <label>
                        <input type="checkbox" class="check-all"/>
                        <span class="checkAllHead">全选</span>
                    </label>
                </div>
                <div class="col-md-4 car-title">商品</div>
                <div class="col-md-1 car-title">单价</div>
                <div class="col-md-2 car-title">数量</div>
                <div class="col-md-1 car-title">金额</div>
                <div class="col-md-2 car-title">操作</div>
            </div>
        </div>
        <!--图书陈列部分-->
        <div class="bookInsert" id="bookInsert"></div>

        <!--底部结算栏-->
        <div class="shoppingCarFoot">
            <div class="car_title_foot bottom-menu-include">
                <div class="col-md-2 check-foot bottom-menu">
                    <label>
                        <input type="checkbox" class="check-all"/>
                        <span class="checkAllFoot">全选</span>
                    </label>
                </div>
                <div class="col-md-2 bottom-menu">

                </div>
                <div class="col-md-3 bottom-menu">
                    <span>已选商品</span>&nbsp;&nbsp;<span class="bookCount">0</span>&nbsp;&nbsp;<span>件</span>
                </div>
                <div class="col-md-3 bottom-menu">
                    <span>合计：</span>¥<span class="bookMoney" id="bookMoney">0.00</span>
                </div>
                <div class="col-md-2 bottom-menu submitData submitDis" id="s_submit">
                    结算
                </div>
            </div>
        </div>
    </div>
</form>
<jsp:include page="../public/public_bottom.jsp"></jsp:include>
</body>

</html>