<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="../img/favicon.ico" mce_href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/order.css" />
    <link rel="stylesheet" href="../css/public_top.css" />
    <link rel="stylesheet" href="../css/public_bottom.css" />
    <title>确认订单</title>
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/distpicker.data.js"></script>
    <script type="text/javascript" src="../js/distpicker.js"></script>
    <script type="text/javascript" src="../js/order.js"></script>
    <style>
        .button_save{
            background: #8C222C;
            color: white;
        }
        .button_save:hover{
            background: #7B111B;
            color: white;
        }
    </style>
</head>
<body>
<jsp:include page="../public/public_top.jsp"></jsp:include>
<input type="hidden" name="userid" id="userid" value="${user.id}">
<!-- 未填写默认地址时，首次填写地址，确认订单页面 -->
<div id="addressDiv">
    <div class="zhu_ti" style="display: none">
        <h4>收货地址</h4>
        <table>
            <tr>
                <td><span class="span">*</span>选择所在地</td>
                <td>
                    <div id="distpicker">
                        <select></select>
                        <select></select>
                        <select></select>
                    </div>
                </td>
            </tr>
            <tr>
                <td><span class="span">*</span>详细地址</td>
                <td><input type="text" name="address" placeholder="请填写详细到门牌号,楼层及房间号的详细地址" /><span></span></td>
            </tr>
            <tr>
                <td>邮政编码</td>
                <td id="celect"><input type="text" name="youZhen" />输入详细地址后
                    <a href="#" onclick="you_bian();">邮局查询</a>
                </td>
            </tr>
            <tr>
                <td><span class="span">*</span>收货人姓名</td>
                <td><input type="text" name="username" id="username" /><span></span></td>
            </tr>
            <tr>
                <td><span class="span">*</span>联系电话</td>
                <td><input type="number" name="phone" placeholder="手机号" /><span></span></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <div>
                        <button class="btn btn-lg btn_buy_1 button_save" id="addAddress">保存并使用</button>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!-- 有默认地址时，展示默认地址 -->
</div>

<!-- 固定的商品清单列表 -->
<div class="commodity_list">
    <h4>商品清单</h4>
    <%--动态拼接商品数据--%>
    <div class="shopList">
        <div class="commodity">
            <div class="commodity_div1">
                <img src="../img/0103.jpg" width="50px" height="50px"/>
            </div>
            <div class="commodity_div2" >汉唐间史学的发展（修订本）</div>
            <div class="commodity_div3">全新</div>
            <div class="commodity_div4">30.00</div>
            <div class="commodity_div5">×1件</div>
            <div class="commodity_div6" style="color: #BF7F5F;">30.00</div>
        </div>
        <div class="express lr">
            <img src="../img/car.png" width="20px" height="20px" />
            运送方式:&nbsp;&nbsp;&nbsp;
            <img src="../img/checkbox.png" width="20px" height="20px" /> 快递￥16.00&nbsp;&nbsp;&nbsp;
            运费：<span style="color: #BF7F5F;">16.00</span>
        </div>
    </div>
    <div class="fen_ge"></div>
    <div class="price">
        共计<span>1</span>件商品,应付总金额<span class="span_price">￥46.00</span>
    </div>
    <div>
        <button class="btn btn-lg btn_buy_1 button_save order">确认并提交订单</button>
    </div>
</div>
<jsp:include page="../public/public_bottom.jsp"></jsp:include>

</body>
</html>
