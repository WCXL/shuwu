<%@ page import="com.kgc.entity.User" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="../img/favicon.ico" mce_href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/bookInfo.css" />
    <link rel="stylesheet" href="../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/public_top.css" />
    <link rel="stylesheet" href="../css/public_bottom.css" />
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/bookInfo.js"></script>
    <title>书籍详情页面</title>
    <style>
        .btn_buy_1{
            border: 1px solid #8C222C;
            color: #8C222C;
            background: #F8F7F3;
        }
        .btn_buy_2:hover{
            color: white;
            background: #7B111B;
        }
        .btn_buy_1:hover{
            color: #8C222C;
            background: #FFFFFF;

        }
        .list{
            padding:20px 0;
            position:relative;

        }
        .comments{

            padding:10px 0;

        }

        .comment-text span {
            color:#eb7350;
        }


        .hf-text {
            display:block;
            height:15px;
            width:840px;
            padding:5px;
            resize:none;
            color:#ccc;
            font-size:15px;
            margin-left: -30px;
        }
        .hf-text {
            height:60px;
            color:#333;
            border:1px solid #ff8140;
        }
        .hf-btn {
            margin-top: 20px;
            margin-left: 730px;
            width:65px;
            height:26px;
            background:#AC2925;
            color:white;
            font-size:12px;
            display:inline;
            border: 0px;
        }

        .heart_case{
            position: relative;
            top: 26px;
            left: 20px;
            display: inline-block;
            border: 1px solid red;
            width: 50px;
            height: 50px;
            text-align: center;
            line-height: 50px;
            background: darkred;
            border-radius:6px ;
        }

        .heart_case:hover{
            cursor: pointer;
        }

        .heart {
            display: inline-block;
            width: 18px;
            height: 18px;
            background-color: white;
            transform: rotate(-45deg);
            margin: 17px 16px;
        }

        .heart:before {
            content: "";
            position: absolute;
            top: -9px;
            left: 0;
            width: 18px;
            height: 18px;
            border-radius: 50%;
            background-color: white;
        }

        .heart:after {
            content: "";
            position: absolute;
            top: 0px;
            left: 9px;
            width: 18px;
            height: 18px;
            background-color: white;
            border-radius: 50%;
        }

        .checkon{
            background-color: tomato;
        }

        .checkon:before{
            background-color: tomato;
        }

        .checkon:after{
            background-color: tomato;
        }

    </style>

</head>
<body>
<jsp:include page="../public/public_top.jsp"></jsp:include>
<div class="div_top"></div>
<div class="div1" style="width: 1000px;margin: 0 auto;margin-top: 10px;">
    <!-- 图片展示div -->
    <input type="hidden" name="userId" id="userId" value="${user.id}">
    <input type="hidden" id="username" value="${user.name}">
    <div class="pic lf">
        <div class="pic_big"><img src="../img/0103.jpg" /></div>
    </div>
    <!-- 正文展示div -->
    <div class="bookInfo lr">
        <div class="title">
            <h4></h4>
        </div>
        <div class="container jian_jie" style="width: 620px;">
            <div class="row">
                <div class="col-md-4">
                    作者:<span class="author"></span><br /> 出版人:<span class="publish"></span>
                    <br /> 年代:民国 (1912-1948)
                </div>
                <div class="col-md-4">
                    纸张:其他<br /> 刻印方式:珂罗版
                    <br /> 装帧:其他 (1912-1948)
                </div>
                <div class="col-md-4">
                    尺寸: 29.5 × 17 × 1.7 cm<br /> 册数: 1册<br />
                </div>
            </div>
        </div>

        <div class="jia_ge container" style="width: 620px;">
            <div class="row">
                <div class="col-md-2">
                    <span>售价</span><br />
                    <span>品相</span>
                </div>
                <div class="col-md-3">
                    <span id="price">￥ <span class="price" id="bookPrice"></span></span><br />
                    <span id="price_next">全新</span>
                </div>
            </div>
        </div>
        <!-- 上架时间和数量 -->
        <div class="time_num container" style="width: 620px;">
            <div class="row">
                <div class="col-md-2">
                    <span>上架时间</span><br />
                    <span>数量</span>
                </div>
                <div class="col-md-4">
                    <span id="book_time">2019-05-15</span><br />
                    <span id="book_num">只剩一件，预购从速</span>
                </div>
            </div>
        </div>
        <%--购买图书数量--%>
        <div class="time_num container shopNumber" style="width: 620px;">
            <div class="row">
                <div class="col-md-2">
                    <span>购买数量</span>
                </div>
                <div class="col-md-4">
                    <input type="button" name="minus" value="-">
                    <input autocomplete="off" readonly  type="text" name="amount" id="amount" value="1" style="text-align: center">
                    <input type="button" name="plus" value="+">
                </div>
            </div>
        </div>
        <!-- 图书内容详情 -->
        <div class="book_intro">
            <h4>内容简介:</h4>
            <div class="book_intro_Info">
            </div>
        </div>

        <!-- 立即购买按钮 -->
        <div class=" div_buy">
              <button onclick="shoping()" class="btn btn-lg btn_buy_1" style="border: 1px solid #8C222C">立即购买</button>
              <button onclick="addShoppingCar()" class="btn btn-lg btn_buy_2"><img src="../img/购物车.png"/>加入购物车</button>
            <span class="heart_case">
		        <span class="heart "></span>
		    </span>
        </div>



    </div>

</div>


<jsp:include page="../comment/comments.jsp"></jsp:include>

<div style="margin-bottom: 30px"></div>
<ul id="pn">
<li class="list">
    <div class="comments">
        <div class="hf" style="margin-left: 40px">
            <textarea type="text" class="hf-text" autocomplete="off" maxlength="100" placeholder="评论……"></textarea>
            <button class="hf-btn">提交</button>
        </div>
    </div>
</li>
</ul>
<jsp:include page="../public/public_bottom.jsp"></jsp:include>
</body>
<script>

    $(function () {

        $.getJSON("../user/confirmFavorite",{"userId": "${user.id}", "bookId": getUrlParam("id")},function (data) {
            if(data){
                $(".heart").addClass("checkon");
            }
        })



        $(".hf-btn").click(function () {
            var bookid=getUrlParam("id")
            var username=$("#username").val();
            var content=$(".hf-text").val();
            var userid=$("#userId").val();

            if (username==""){
                alert("登录后才能进行评论！")
                window.location.href='../login_register/login_register.jsp';
            }else if (content==""){
                alert("评论不能为空！")
            } else {
            $.getJSON("../comment/addcomment",{"bookid":bookid,"username":username,"content":content,"userid":userid},function () {
               window.location.reload();
            })
                }
        })

        $(".heart_case").bind("click",function () {
           if("" =="${user.id}"){
               $("#loginmodal").modal("show");
               return;
           }
            if($(".heart").hasClass("checkon")){
                if(confirm("确认移除收藏？")) {
                    $(".heart").removeClass("checkon")
                    $.getJSON("../user/delFavorite", {"userId": "${user.id}", "bookId": getUrlParam("id")}, function (data) {
                        if (!data) {

                            alert("你还未收藏！");
                        }
                    });
                }
            }else {
                $(".heart").addClass("checkon");
                $.getJSON("../user/addFavorite", {"userId": "${user.id}", "bookId": getUrlParam("id")}, function (data) {
                    if (data) {
                        alert("已加入收藏！")
                    } else {
                        alert("商品已加入收藏，无法继续添加");
                    }
                });
            }
        })
    })
</script>
</html>
